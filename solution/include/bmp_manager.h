//#pragma once // предотвращает повторный импорт
#include "image.h"
#include <stdint.h>
#include <stdio.h>

#ifndef BMP_READER_H
#define BMP_READER_H


typedef struct __attribute__((packed)) BMP_Header
{
    uint16_t bfType; // Отметка для отличия формата от других (сигнатура формата).
    uint32_t bfileSize; // Размер файла в байтах. 
    uint32_t bfReserved; // 1 и 2 Зарезервированы и должны содержать ноль. 
    uint32_t bOffBits; // Положение пиксельных данных относительно начала данной структуры (в байтах). 
    uint32_t biSize; // Размер данной структуры в байтах, указывающий также на версию структуры.
    uint32_t biWidth; // Ширина (bcWidth) и высота (bcHeight) растра в пикселях. 
    uint32_t biHeight; // ---Указываются целым числом без знака. Значение 0 не документировано.
    uint16_t biPlanes; // В BMP допустимо только значение 1. Это поле используется в значках и курсорах Windows. 
    uint16_t biBitCount; // Количество бит на пиксель
    uint32_t biCompression; // Указывает на способ хранения пикселей
    uint32_t biSizeImage; // Размер пиксельных данных в байтах. Может быть обнулено если хранение осуществляется двумерным массивом. 
    uint32_t biXPelsPerMeter; // Количество пикселей на метр по горизонтали  
    uint32_t biYPelsPerMeter; // --- и вертикали
    uint32_t biClrUsed; // Размер таблицы цветов в ячейках. 
    uint32_t biClrImportant; // Количество ячеек от начала таблицы цветов до последней используемой (включая её саму). 
} BMP_Header;




// Каждый входной формат описывается в отдельном модуле; 
// они предоставляют функции для считывания файлов разных форматов в struct image и для записи на диск в тех же форматах.
// Соответственно это модуль для .bmp

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ERROR
  /* коды других ошибок  */
  };

enum read_status from_bmp( FILE* in, struct Image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct Image const* img );

#endif
