#include <stdlib.h>
#include <stdint.h>


#ifndef IMAGE
#define IMAGE

typedef struct Image {
  uint64_t width, height;
  struct Pixel* data;
} Image;

typedef struct Pixel { uint8_t b, g, r; } Pixel;

Image* init_image(void); // инициализируем память под изображение

void free_image(struct Image *image); // освобождаем память (как на практике)

void print_img(Image* image);

#endif
