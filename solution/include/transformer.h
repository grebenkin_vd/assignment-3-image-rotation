#include "image.h"
#include <stdint.h>

#ifndef TRANSFORMER
#define TRANSFORMER

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct Image rotate( struct Image const source );


#endif
