#include "transformer.h"

struct Image rotate( struct Image const source ){
    Image* rotated_img = init_image();
    rotated_img->width = source.height;
    rotated_img->height = source.width;
    uint64_t width = source.width;
    uint64_t height = source.height;
    rotated_img->data = (Pixel*)malloc(sizeof(Pixel)*width*height); // создали шаблон для заполнения
    // заполняем
    for (uint64_t i = 0; i < source.width; i++){
        for (uint64_t j = 0; j < source.height; j++){
            rotated_img->data[i * height + j] = source.data[(height - j - 1)* width + i];
        }
    }
    Image r_img = *rotated_img;
    free(rotated_img);
    return r_img;
}
