#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


#ifndef IMAGE_C
#define IMAGE_C

Image* init_image(void){
  Image* image = (Image*)malloc(sizeof(Image));
  image->width = 0;
  image->height = 0;
  return image;
} // инициализируем память под изображение

void print_img(Image* image){
  for (int32_t i = 0; i < image->height; i++){ // итератор по строкам
    for (int32_t j = 0; j < image->width; j++){ // итератор по строке
            Pixel p = image->data[i*image->width + j];
            printf("[%d %d %d]", p.r, p.g, p.b);
        }
    printf("\n");            
  }
}

void free_image(Image* image){
  free(image->data);
  free(image);
}
#endif
