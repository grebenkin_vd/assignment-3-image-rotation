#include "bmp_manager.h"
#include "image.h"
#include "transformer.h"
#include <stdio.h>
#include <stdlib.h>

void read_react(enum read_status status){
    if (status == READ_OK) return;
    if (status == READ_ERROR){
        fprintf(stderr, "unable to read file");
        exit(2);
    }
    else if (status == READ_INVALID_SIGNATURE){
        fprintf(stderr, "invailid signature exeption");
        exit(1);
    }
    else if (status == READ_INVALID_HEADER){
        fprintf(stderr, "invailid header exeption");
        exit(1);
    }
    else if (status == READ_INVALID_BITS){
        fprintf(stderr, "invailid bits exeption");
        exit(1);
    }
} // stoppint programm if error exit status

void write_react(enum write_status status){
    if (status == WRITE_OK) return;
    if (status == WRITE_ERROR){
        fprintf(stderr, "unable to read file");
        exit(1);
    }
} // stoppint programm if error exit status

int main(int argc, char* argv[]){
    // проверим корректность аргументов
    if (argc != 3){
        printf("Wrong input - ./image-transformer <source-image> <transformed-image>\n");
        return 128; // код выхода неверного аргумента
    }

    char* src = argv[1];
    FILE* file = fopen(src, "r"); 
    Image* image = init_image();

    enum read_status status = from_bmp(file, image);
    fclose(file); // закрываем считываемый файл
    read_react(status);
    Image rotated_img = rotate(*image);
    //произведём запись в файл
    src = argv[2]; // путь к файлу вывода
    FILE* outfile = fopen(src, "w");
    enum write_status wstatus = to_bmp(outfile, &rotated_img); // поворачивам на 90* против часовой
    fclose(outfile); // закрываем файл для записи
    
    free_image(image);
    free(rotated_img.data);

    write_react(wstatus);
    return 0;
}

