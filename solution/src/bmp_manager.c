#include "bmp_manager.h"
#include <stdio.h>
#include <stdlib.h>

#define BMP3_TYPE 19778
#define BMP3_RESERVED 0
#define BMP3_BISIZE 40
#define BMP3_PLANES 1
#define BMP3_BITCOUNT 24
#define BMP3_COMPRESSION 0
#define BMP3_PIX_PER_METER 2834
#define BMP3_CLR 0

#ifndef BMP_MANAGER_C
#define BMP_MANAGER_C


void trass_file(BMP_Header* const header){
    printf("bfType %d\n", header->bfType);
    printf("bfileSize %d\n", header->bfileSize);
    printf("bfReserved %d\n", header->bfReserved);
    printf("bOffBits %d\n", header->bOffBits);
    printf("biSize %d\n", header->biSize);
    printf("biWidth %d\n", header->biWidth);
    printf("biHeight %d\n", header->biHeight);
    printf("biPlanes %d\n", header->biPlanes);
    printf("biBitCount %d\n", header->biBitCount);
    printf("biCompression %d\n", header->biCompression);
    printf("biSizeImage %d\n", header->biSizeImage);
    printf("biXPelsPerMeter %d\n", header->biXPelsPerMeter);
    printf("biYPelsPerMeter %d\n", header->biYPelsPerMeter);
    printf("biClrUsed %d\n", header->biClrUsed);
    printf("biClrImportant %d\n", header->biClrImportant);
}

uint32_t fill_header(BMP_Header* header, uint32_t width, uint32_t height){
    uint32_t bytes_per_pixel = 3;
    uint32_t line_byte_size = bytes_per_pixel * width;
    uint32_t line_padding = (4 - (line_byte_size % 4)) % 4;  // выравнивание
    line_byte_size = line_byte_size + line_padding;

    header->bfType = BMP3_TYPE;
    header->bfileSize = 0; // пропустим file size, зададим в конце 
    header->bfReserved = BMP3_RESERVED;
    header->bOffBits = sizeof(BMP_Header); // в байтах :)
    header->biSize = BMP3_BISIZE; // размер структуры
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = BMP3_PLANES;
    header->biBitCount = BMP3_BITCOUNT;
    header->biCompression = BMP3_COMPRESSION;
    header->biSizeImage = height * line_byte_size;
    header->biXPelsPerMeter = BMP3_PIX_PER_METER;
    header->biYPelsPerMeter = BMP3_PIX_PER_METER; // хз как почитать это
    header->biClrUsed = BMP3_CLR;
    header->biClrImportant = BMP3_CLR;
    header->bfileSize = header->biSizeImage + header->bOffBits; // +
    return line_padding;
}

void free_header(BMP_Header* header){
    free(header);
}

enum read_status read_bmp_pixels(FILE* in, struct Image* img, BMP_Header* header){
    // при передаче файла, указатель должен быть на начале массива
    img->width = header->biWidth;
    img->height = header->biHeight;
    // считываем данные о пикселях
    uint16_t bytes_per_pixel = header->biBitCount / 8;
    uint16_t line_byte_size = bytes_per_pixel * header->biWidth;
    uint16_t line_padding = (4 - (line_byte_size % 4)) % 4;  // учтём выравнивание
    if (bytes_per_pixel < 3 || bytes_per_pixel > 4){ return READ_INVALID_BITS; }

    img->data = (Pixel*)malloc(sizeof(Pixel)*header->biWidth*header->biHeight);
    fseek(in, header->bOffBits, SEEK_SET);
    if (bytes_per_pixel == 3){ // наша основная задача
        for (uint32_t i = 0; i < header->biHeight; i++){ // итератор по строкам
            for (uint32_t j = 0; j < header->biWidth; j++){ // итератор по строке
                    if (! fread(&img->data[(i*header->biWidth + j)], sizeof(Pixel), 1, in)){
                        return READ_INVALID_BITS;
                    }
                }
                fseek(in, line_padding, SEEK_CUR);
        }
    }
    if (bytes_per_pixel == 4){ // необязательный случай, но пусть тоже умеет считывать (4байтные)
        for (uint32_t i = 0; i < header->biHeight; i++){ // итератор по строкам
            for (uint32_t j = 0; j < header->biWidth; j++){ // итератор по строке
                    if(! fread(&img->data[(i*header->biWidth + j)], sizeof(Pixel), 1, in)){
                        return READ_INVALID_BITS;
                    }
                    fseek(in, 1, SEEK_CUR);
                }
        }
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct Image* img ){ // функция чтения bmp
    if (!in){
        return READ_ERROR;
    } // Проверяем файл на доступность
    BMP_Header* header = (BMP_Header*)malloc(sizeof(BMP_Header)); // выделяем память под хэдер
    if (fread(header, sizeof(BMP_Header), 1, in)){
                        printf("read");
                    } // читаем хэдер  
    if (!(header->bfileSize > 0 && // проверяем корректность считанного хедера
     header->biSize > 0 &&
     header->biWidth > 0 && header->biHeight > 0 &&
     header->bOffBits > 0 && header->bfReserved == 0
     )) return READ_INVALID_HEADER;
    // иначе продолжаем чтение
    fseek(in, header->bOffBits, SEEK_SET); // учтём смещение массива пикселей
    enum read_status status = read_bmp_pixels(in, img, header);
    free_header(header); // освободим хэдер
    return status;
}

enum write_status to_bmp( FILE* out, struct Image const* img ){
    if (!out){
        return WRITE_ERROR;
    } // Проверяем файл на доступность
    BMP_Header* header = (BMP_Header*)malloc(sizeof(BMP_Header));
    uint32_t padding = fill_header(header, img->width, img->height); //заполним хэдер
    if (! fwrite(header, sizeof(BMP_Header), 1, out)){free_header(header); return WRITE_ERROR;}
    free_header(header);

    for (uint64_t i = 0; i < img->height; i++){
        for (uint64_t j = 0; j < img->width; j++){
            if (!fwrite(&img->data[i * img->width + j], sizeof(Pixel), 1, out)){
                return WRITE_ERROR;
            }
        }
        fseek(out, padding, SEEK_CUR); // добавим выравнивание
    }   
    return WRITE_OK;
}

#endif
